<?php


namespace NoTee;


interface TemplateInterface
{
    public function render(string $template): NodeInterface;
}