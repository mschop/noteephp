<?php


namespace NoTee;


class Template implements TemplateInterface
{
    protected AbstractNodeFactory $nodeFactory;
    protected array $templateDirs;

    /**
     * Template constructor.
     * @param array $templateDirs
     * @param AbstractNodeFactory $nodeFactory
     */
    public function __construct(array $templateDirs, AbstractNodeFactory $nodeFactory)
    {
        $this->templateDirs = $templateDirs;
        $this->nodeFactory = $nodeFactory;
    }

    public function render(string $template): NodeInterface
    {
        $template = trim($template, '/\\');
        $dirs = array_reverse($this->templateDirs);
        foreach ($dirs as $dir) {
            $path = $dir . '/' . $template;
            if (file_exists($path)) {
                $return = require($path);
                if ($return !== 1) {
                    return $return;
                }
            }
        }
        throw new \InvalidArgumentException("Template File '$template' not found");
    }
}